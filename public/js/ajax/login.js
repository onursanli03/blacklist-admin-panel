$(function () { // document ready

    var $login_btn = $('#btn-login'),
        $mesaj = $('#alert-message');

    $login_btn.click(function () {
        $mesaj.addClass('hidden');
        $.ajax({
            //url: '/controller/method',	    // istek yapacağın ve sana sonuç dönecek url
            url: '/site/login',     // örnek url, ip bilgisi dönüyor..

            method: 'POST',				        // default u 'GET'.


            data: {						        // önyüzden gidecek parametreler
                email: $('#email').val(),
                password: $('#password').val()
            }

        }).done(function (data) {		// status 200 OK dönünce yani başarılı
            console.log(data);          // şimdilik gelen i görme amaçlı console a yazdıralım

            if(data !== null){
                var jsonObj = $.parseJSON(data);
                if(jsonObj['status'] == 'ok'){
                    window.location.replace(jsonObj['url']);
                }else{
                    $mesaj.removeClass('hidden');
                    $mesaj.html(jsonObj['status-message']);
                }
            }


            // önyüz e başarılı dönülmüş, gelen data yı işleyebilirsin..
            //$mesaj.html(data.ip); // örneğin

        }).fail(function (jqXHR, textStatus, errorThrown) { // status 200 OK dışında bir hata status ü dönerse (500, 503, ... gibi)
            console.error('Request failed: ' + textStatus); // şimdilik console a error olarak yazdıralım

            // gelen hata ya göre aksiyon..
            //$mesaj.html(textStatus); // örneğin

        });
    });
});