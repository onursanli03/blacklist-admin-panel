<?php


/**
 * Class Helper
 */
class Helper{

    /**
     * Checks the given string is null or empty
     * @param $string
     * @return bool
     */
    public static function isNullOrEmpty($string){
        return $string == null || strlen(trim($string)) == 0;
    }

    public static function isMsisdnValid($msisdn){
        $isValid = false;
        if(!Helper::isNullOrEmpty($msisdn)){
            if(substr($msisdn, 0, 1) == "5" && strlen($msisdn) == 10){
                $isValid = true;
            }
        }

        return $isValid;
    }


    /**
     * Executes given statement on the application database.
     * @param $sqlStatement
     * @param array $params
     */
    public static function executeSP($sqlStatement, array $params){
        $db = DB::connection();
        $pdo = $db->getPdo();
        $stmt = $pdo->prepare($sqlStatement);
        foreach($params as $key => $value){
         echo $key .'=>'. $value .'<br/>';
            $stmt->bindParam($key, $value);
        }
        $stmt->execute();
    }
}

