<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', function()
{
	return Redirect::to('/site/login');
});


Route::get('/login', function()
{
	return Redirect::to('/site/login');
});

Route::post('/login', function()
{
	return Redirect::to('/site/login');
});

Route::controller('site', 'HomeController');

//Route::group(array('before' => 'auth'), function()
//{
//
//
//
//});

Route::group(array('before'=>'perm'), function() {

	Route::any('/dashboard/search', array('as' => 'profile', 'uses' => 'DashboardController@search'));

	Route::any('/blacklist/search/{serviceId}', array('as' => 'profile', 'uses' => 'BlacklistController@search'));


	Route::controller('dashboard', 'DashboardController');

	Route::controller('account', 'AccountController');



	Route::controller('/service', 'ServiceController');


	Route::controller('/blacklist', 'BlacklistController');

});