<?php

return array(

    'tr' => [
        'process.success' => 'İşleminiz başarıyla tamamlandı.',
        'process.fail' => 'İşlem başarısız!.',
        'page.not.found' => 'Sayfa bulunamadı!.',
        'already.exist' => 'Zaten mevcut',
        'msisdn.not.valid'=>'Girilen MSISDN geçerli bir formatta değil. (5 ile başlamalı ve 10 haneli olmalı.)'
    ]
);
