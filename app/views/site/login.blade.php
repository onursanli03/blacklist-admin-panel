@extends('layouts.login')


@section('content')
    <!-- Login -->

    <div class="lc-block toggled" id="l-login">
       @if(isset($result))
            <div class="row">
                <div class="col-lg-12" >
                    <div class="alert alert-danger alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        {{$result['status-message']}}
                    </div>
                </div>
            </div>
        @endif
        {{ Form::open(array("url" => "site/login"))}}
        <div class="input-group m-b-20">
            <span class="input-group-addon"><i class="md md-person"></i></span>

            <div class="fg-line">
                {{ Form::text('email', Input::old('email'), array('placeholder' => 'user@example.com', 'autofocus' => true, 'class' => 'form-control', 'id' => 'email')) }}
            </div>
            <div class="form-error">{{ $errors->first('email') }}</div>
        </div>

        <div class="input-group m-b-20">
            <span class="input-group-addon"><i class="md md-accessibility"></i></span>

            <div class="fg-line">
                {{ Form::password('password', array('class' => 'form-control', 'placeholder'=>'Password', 'id' => 'password')) }}
            </div>
            <div class="form-error">{{ $errors->first('password') }}</div>
        </div>

        <div class="clearfix"></div>

        <button type="submit" class="btn btn-login btn-danger btn-float"><i class="md md-arrow-forward"></i></button>
        {{ Form::close() }}
    </div>

@stop