@extends('layouts.master')

@section('content')

    <div class="row">
        <div class="col-sm-6">
            <div class="card">
                <div class="card-header">
                    <h2>About
                        <small>Extend form controls by adding text or buttons before, after, or on both sides of any
                            text-based
                            inputs.
                        </small>
                    </h2>
                </div>

                <div class="card-body card-padding">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="md md-person"></i></span>
                                    <div class="col-sm-12">
                                        <div class="col-sm-6">
                                            <div class="fg-line">
                                                <input type="text" placeholder="First Name" class="form-control">
                                            </div>
                                        </div>
                                        <div class=" col-sm-6">
                                            <div class="fg-line">

                                            <input type="text" placeholder="Last Name" class="form-control">
                                            </div>
                                        </div>
                                    </div>
                            </div>

                            <br>

                            <div class="input-group">
                                <span class="input-group-addon"><i class="md md-star"></i></span>

                                <div class="fg-line">
                                    <input type="text" placeholder="Title" class="form-control">
                                </div>
                            </div>

                            <br>

                            <div class="input-group">
                                <span class="input-group-addon"><i class="md md-local-phone"></i></span>

                                <div class="fg-line">
                                    <input type="text" placeholder="Contact Number" class="form-control">
                                </div>
                            </div>

                            <br>

                            <div class="input-group">
                                <span class="input-group-addon"><i class="md md-location-on"></i></span>

                                <div class="fg-line">
                                    <input placeholder="Home Address" class="form-control">
                                </div>
                            </div>

                            <br>

                            <button class="btn btn-primary waves-effect">Save</button>

                        </div>

                    </div>
                </div>

            </div>
        </div>




        <div class="col-sm-6">
            <div class="card">
                <div class="card-header">
                    <h2>Change your password
                        <small>Extend form controls by adding text or buttons before, after, or on both sides of any
                            text-based
                            inputs.
                        </small>
                    </h2>
                </div>


                <div class="card-body card-padding">

                    <div class="row">
                        <div class="col-sm-12">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="md md-person"></i></span>

                                <div class="fg-line">
                                    <input type="text" placeholder="user@example.com" class="form-control">
                                </div>
                            </div>

                            <br>

                            <div class="input-group">
                                <span class="input-group-addon"><i class="md md-vpn-key"></i></span>

                                <div class="fg-line">
                                    <input type="password" placeholder="Current password" class="form-control">
                                </div>
                            </div>

                            <br>

                            <div class="input-group">
                                <span class="input-group-addon"><i class="md md-new-releases"></i></span>

                                <div class="fg-line">
                                    <input type="password" placeholder="New password" class="form-control">
                                </div>
                            </div>

                            <br>

                            <div class="input-group">
                                <span class="input-group-addon"><i class="md md-repeat"></i></span>

                                <div class="fg-line">
                                    <input type="password" placeholder="Verify new password" class="form-control">
                                </div>
                            </div>
                            <br>


                            <button class="btn btn-warning waves-effect">Change</button>

                        </div>

                    </div>


                </div>

            </div>
        </div>

    </div>


    <div class="row">
        <div class="col-sm-6">
            <div class="card">
                <div class="card-header">
                    <h2>Change Profile Photo
                        <small>Extend form controls by adding text or buttons before, after, or on both sides of any
                            text-based
                            inputs.
                        </small>
                    </h2>
                </div>

                <div class="card-body card-padding">
                    <div class="row">
                        <div class="col-sm-12">
                            <div data-provides="fileinput" class="fileinput fileinput-new">
                                <div data-trigger="fileinput" class="fileinput-preview thumbnail"></div>
                                <div>
                                    <span class="btn btn-info btn-file waves-effect waves-button">
                                        <span class="fileinput-new">Select image</span>
                                        <span class="fileinput-exists">Change</span>
                                        <input type="file" name="...">
                                    </span>
                                    <a data-dismiss="fileinput" class="btn btn-danger fileinput-exists waves-effect waves-button" href="#">Remove</a>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

            </div>
        </div>
    </div>
    @section('footer')
    @parent
    {{ HTML::script('/vendors/fileinput/fileinput.min.js') }}

@stop

@stop