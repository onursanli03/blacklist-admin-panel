<!DOCTYPE html>
<!--[if IE 9 ]><html class="ie9"><![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Blacklist Admin</title>

    <!-- Vendor CSS -->
    <link href="/vendors/bootgrid/jquery.bootgrid.min.css" rel="stylesheet">
    <link href="/vendors/animate-css/animate.min.css" rel="stylesheet">

    <!-- CSS -->
    <link href="/css/app.min.css" rel="stylesheet">
</head>
<body>
<header id="header">
    <ul class="header-inner">
        <li id="menu-trigger" data-trigger="#sidebar">
            <div class="line-wrap">
                <div class="line top"></div>
                <div class="line center"></div>
                <div class="line bottom"></div>
            </div>
        </li>

        <li class="logo hidden-xs">
            <a href="/dashboard/index">BLACKLIST ADMIN</a>
        </li>

        <li class="pull-right">
            <ul class="top-menu">
                <li id="toggle-width">
                    <div class="toggle-switch">
                        <input id="tw-switch" type="checkbox" hidden="hidden">
                        <label for="tw-switch" class="ts-helper"></label>
                    </div>
                </li>

                <li class="dropdown">
                    <a data-toggle="dropdown" class="tm-settings" href=""></a>

                </li>
            </ul>
        </li>
    </ul>
    <!-- Top Search Content -->
    <div id="top-search-wrap">
        <input type="text">
        <i id="top-search-close">&times;</i>
    </div>
</header>

<section id="main">
    <aside id="sidebar">
        <div class="sidebar-inner">
            <div class="profile-menu">
                <a href="" data-toggle="dropdown">
                    <div class="profile-pic">

                    </div>

                    <div class="profile-info">
                        {{ Sentry::getUser()->first_name}} {{ Sentry::getUser()->last_name }}

                        <i class="md md-arrow-drop-down"></i>
                    </div>
                </a>

                <ul class="main-menu">
                    <li>
                        <a href="/site/logout"><i class="md md-history"></i> Çıkış</a>
                    </li>
                </ul>
            </div>

            <ul class="main-menu">
                <li><a href="/dashboard/index"><i class="md md-home"></i> Anasayfa</a></li>

            </ul>
        </div>
    </aside>


    <section id="content">
        <div class="container">
            <?php if(Session::get('success') !== null) : ?>
            <div class="row">
                <div class="col-lg-12" >
                    <div class="alert alert-success alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <?php  echo Session::get('success') ?>
                    </div>
                </div>
            </div>
            <hr/>
            <?php endif; ?>
            <?php if(Session::get('alert') !== null) : ?>
            <div class="row">
                <div class="col-lg-12" >
                    <div class="alert alert-danger alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <?php  echo Session::get('alert') ?>
                    </div>
                </div>
            </div>
            <hr/>
            <?php endif; ?>
            @yield('content')
        </div>
    </section>
</section>





@section('footer')


    <!-- Javascript Libraries -->
    <script src="/js/jquery-2.1.1.min.js"></script>
    <script src="/js/bootstrap.min.js"></script>

    <script src="/vendors/nicescroll/jquery.nicescroll.min.js"></script>
    <script src="/vendors/bootgrid/jquery.bootgrid.min.js"></script>
    <script src="/vendors/waves/waves.min.js"></script>
    <script src="/vendors/bootstrap-growl/bootstrap-growl.min.js"></script>
    <script src="/vendors/sweet-alert/sweet-alert.min.js"></script>

    <script src="/js/functions.js"></script>

@show

</body>
</html>