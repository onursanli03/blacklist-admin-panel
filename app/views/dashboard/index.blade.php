@extends('layouts.master')


@section('content')

    <div class="welcome-content">
        <div class="mini-charts">
            <div class="row">
                <div class="col-sm-6 col-md-4">
                    <div class="mini-charts-item bgm-cyan">
                        <div class="clearfix">
                            <div class="chart stats-bar">
                                <canvas style="display: inline-block; width: 83px; height: 45px; vertical-align: top;"
                                        width="83" height="45"></canvas>
                            </div>
                            <div class="count">
                                <small>Servis Sayısı</small>
                                <h2>{{ $serviceCount }}</h2>
                            </div>
                        </div>
                    </div>
                </div>



                <div class="col-sm-6 col-md-4">
                    <div class="mini-charts-item bgm-orange">
                        <div class="clearfix">
                            <div class="chart stats-line">
                                <canvas style="display: inline-block; width: 85px; height: 45px; vertical-align: top;"
                                        width="85" height="45"></canvas>
                            </div>
                            <div class="count">
                                <small>Blacklist Tipi Sayısı</small>
                                <h2>{{ $channelTypeCount }}</h2>

                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6 col-md-4">
                    <div class="mini-charts-item bgm-lightgreen">
                        <div class="clearfix">
                            <div class="chart stats-bar-2">
                                <canvas style="display: inline-block; width: 83px; height: 45px; vertical-align: top;"
                                        width="83" height="45"></canvas>
                            </div>
                            <div class="count">
                                <small>Blacklist Sayısı</small>
                                <h2>{{ $blacklistCount }}</h2>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>


    <div class="blacklist-services">
        <div class="card">
            <div class="card-header">
                <div class="col-sm-6">
                    <h2>Msisdn Ara</h2>
                </div>
            </div>
            <div class="bootgrid-header container-fluid" id="data-table-command-header">
                {{ Form::open(array('url' => 'dashboard/msisdn-search')) }}
                <div class="row">
                    <div class="col-sm-4 actionBar">
                        <div class="search form-group">
                            <div class="input-group">
                                <span class="md icon input-group-addon glyphicon-search"></span>
                                {{ Form::text('name', Input::old('name'), array('autofocus' => true, 'class' => 'search-field form-control', 'placeholder' => 'MSISDN Ara')) }}
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <button type="submit" class="btn btn-success waves-effect">ARA</button>
                    </div>
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>




    <div class="blacklist-services">
        <div class="card">
            <div class="card-header">
                <div class="col-sm-6">
                    <h2>Blacklist Servis Listesi</h2>
                </div>

                <div class="col-sm-6">
                    <a href="/service/create" class="btn btn-primary waves-effect pull-right">SERVIS OLUŞTUR</a>
                </div>
            </div>


            <div class="bootgrid-header container-fluid" id="data-table-command-header">
                <div class="row">


                    <div class="col-sm-6 actionBar">
                        {{ Form::open(array('url' => 'dashboard/search')) }}
                        <div class="search form-group">
                            <div class="input-group">
                                <span class="md icon input-group-addon glyphicon-search"></span>
                                {{ Form::text('name', Input::old('name'), array('autofocus' => true, 'class' => 'search-field form-control', 'placeholder' => 'Ad Ara')) }}
                            </div>
                        </div>
                        {{ Form::close() }}
                    </div>

                </div>
            </div>
            <table class="table table-striped table-vmiddle bootgrid-table" id="data-table-command" aria-busy="false">
                <thead>
                <tr>
                    <th class="text-left" data-column-id="id"><a class="column-header-anchor sortable"
                                                                 href="javascript:void(0);"><span class="text">ID</span><span
                                    class="md icon "></span></a></th>
                    <th class="text-left" data-column-id="sender"><a class="column-header-anchor sortable"
                                                                     href="javascript:void(0);"><span class="text">Adı</span><span
                                    class="md icon "></span></a></th>
                    <th class="text-left" data-column-id="sender"><a class="column-header-anchor sortable"
                                                                     href="javascript:void(0);"><span class="text">Kullanıcı Adı</span><span
                                    class="md icon "></span></a></th>
                    <th class="text-left" data-column-id="received"><a class="column-header-anchor sortable"
                                                                       href="javascript:void(0);"><span class="text">Durum</span><span
                                    class="md icon"></span></a></th>
                    <th class="text-left" data-column-id="commands"><a class="column-header-anchor "
                                                                       href="javascript:void(0);"><span class="text"></span><span
                                    class="md icon "></span></a></th>
                </tr>
                </thead>
                <tbody>
                    @if(@isset($services) and @count($services))
                        <?php $i=0 ?>
                        @foreach($services as $service)
                            <tr data-row-id="<?php echo $i; ?>">
                                <td class="text-left">{{ $service->id }}</td>
                                <td class="text-left">{{ $service->name }}</td>
                                <td class="text-left">{{ $service->username }}</td>
                                <td class="text-left">{{ $service->status }}</td>
                                <td class="text-left">
                                    <a href="/service/update/{{ $service->id }}" data-row-id="{{$service->id}}" class="btn btn-icon command-delete">
                                        <span class="md md-edit m-t-5"></span>
                                    </a>
                                    <a href="/blacklist/list/{{ $service->id }}" data-row-id="{{$service->id}}" class="btn btn-icon command-delete"><span
                                                class="md md-arrow-forward m-t-5"></span></a>
                                </td>
                            </tr>
                            <?php $i++ ?>
                        @endforeach
                    @else
                        <tr>
                            <td class="text-left" colspan="4">Veri Bulunamadı.</td>

                        </tr>

                    @endif

                </tbody>
            </table>
            <div class="bootgrid-footer container-fluid" id="data-table-command-footer">
                <div class="row">
                    <div class="col-sm-6">
                        <?php echo $services->links(); ?>
                    </div>
                </div>
            </div>
        </div>

    </div>

@section('footer')
    {{ HTML::script('/js/jquery-2.1.1.min.js') }}
    {{ HTML::script('/js/bootstrap.min.js') }}
    {{ HTML::script('/vendors/flot/jquery.flot.min.js') }}
    {{ HTML::script('/vendors/flot/jquery.flot.resize.min.js') }}
    {{ HTML::script('/vendors/flot/plugins/curvedLines.js') }}
    {{ HTML::script('/vendors/sparklines/jquery.sparkline.min.js') }}
    {{ HTML::script('/vendors/easypiechart/jquery.easypiechart.min.js') }}
    {{ HTML::script('/vendors/fullcalendar/lib/moment.min.js') }}
    {{ HTML::script('/vendors/fullcalendar/fullcalendar.min.js') }}
    {{ HTML::script('/vendors/simpleWeather/jquery.simpleWeather.min.js') }}
    {{ HTML::script('/vendors/auto-size/jquery.autosize.min.js') }}
    {{ HTML::script('/vendors/nicescroll/jquery.nicescroll.min.js') }}
    {{ HTML::script('/vendors/waves/waves.min.js') }}
    {{ HTML::script('/vendors/bootstrap-growl/bootstrap-growl.min.js') }}
    {{ HTML::script('/vendors/sweet-alert/sweet-alert.min.js') }}
    {{ HTML::script('/js/flot-charts/curved-line-chart.js') }}
    {{ HTML::script('/js/flot-charts/line-chart.js') }}
    {{ HTML::script('/js/charts.js') }}
    {{ HTML::script('/js/functions.js') }}
@stop

@stop

