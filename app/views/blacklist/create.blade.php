@extends('layouts.master')


@section('content')
@include('common._service_info')
<div class="card">
    <div class="card-header">
        <h2>Blacklist | MSISDN Ekle</h2>
    </div>
    <div class="card-body card-padding">
        {{ Form::open(array("url" => "blacklist/create/".$service->id))}}
        <div class="col-md-12">
            <div class="form-group">
                {{ Form::label('msisdn', 'MSISDN') }}
                {{ Form::text('msisdn', Input::old('msisdn'), array('autofocus' => true, 'class' => 'form-control', 'placeholder' => '5439876543')) }}
                <div class="form-error">{{ $errors->first('msisdn') }}</div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-3" style="margin-left: 30px;">
                <p>{{ Form::submit('Ekle', array('class' => 'btn btn-primary')) }}</p>
            </div>
        </div>
        {{ Form::close() }}
    </div>
</div>

@stop