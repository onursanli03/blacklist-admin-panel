@extends('layouts.master')


@section('content')
    @include('common._service_info')

    <div class="card">
        <div class="card-header">
            <h2>Blacklist - Excel Import</h2>
        </div>

        <div class="card-body card-padding">
            <div class="row">
                <div class="col-sm-4">
                    {{ Form::model($service, array('url' => array('blacklist/excel-import', $service->id) , 'files' => 'true')) }}
                    <div class="form-group">
                        {{ Form::label('excel', 'Excel') }}
                        {{ Form::file('excel') }}
                        <div class="form-error">{{ $errors->first('excel') }}</div>
                    </div>
                    <div class="form-group">
                        <div class="btn-group" role="group" aria-label="...">
                            {{ Form::submit('Gönder', array('class' => 'btn btn-primary',  'name'=>'upload-btn', 'id'=>'upload-btn', 'onclick' => '$(this).hide();$("#uploading").show()')) }}

                        </div>
                        <span id="uploading" style="display: none;">Lütfen Bekleyiniz...</span>
                    </div>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
@stop