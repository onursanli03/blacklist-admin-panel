@extends('layouts.master')


@section('content')
    @include('common._service_info')

    <div class="blacklist">
        <div class="card">
            <div class="card-header">
                <div class="col-sm-6">
                    <h2>Blacklist</h2>
                </div>

                <div class="col-sm-6">
                    <div class="btn-group pull-right">
                        <a href="/blacklist/excel-import/{{$service->id}}" class="btn btn-primary waves-effect pull-right">Excel Import</a>
                        <a href="/blacklist/create/{{$service->id}}" class="btn btn-success waves-effect pull-right">MSISDN Ekle</a>
                    </div>
                </div>
            </div>


            <div class="bootgrid-header container-fluid" id="data-table-command-header">
                <div class="row">


                    <div class="col-sm-6 actionBar">
                        {{ Form::open(array('url' => 'blacklist/search/' . $service->id)) }}
                        <div class="search form-group">
                            <div class="input-group">
                                <span class="md icon input-group-addon glyphicon-search"></span>
                                {{ Form::text('msisdn', Input::old('msisdn'), array('autofocus' => true, 'class' => 'search-field form-control', 'placeholder' => 'MSISDN Ara')) }}
                            </div>
                        </div>
                        {{ Form::close() }}
                    </div>

                </div>
            </div>
            <table class="table table-striped table-vmiddle bootgrid-table" id="data-table-command" aria-busy="false">
                <thead>
                <tr>
                    <th class="text-left" data-column-id="id"><a class="column-header-anchor"
                                                                 href="javascript:void(0);"><span class="text">ID</span><span
                                    class="md icon "></span></a></th>
                    <th class="text-left" data-column-id="sender"><a class="column-header-anchor"
                                                                     href="javascript:void(0);"><span class="text">MSISDN</span><span
                                    class="md icon "></span></a></th>
                    <th class="text-left" data-column-id="received"><a class="column-header-anchor"
                                                                       href="javascript:void(0);"><span class="text">Tipi</span><span
                                    class="md icon"></span></a></th>
                    <th class="text-left" data-column-id="commands"><a class="column-header-anchor "
                                                                       href="javascript:void(0);"><span class="text">Oluşturma Tarihi</span><span
                                    class="md icon "></span></a></th>
                </tr>
                </thead>
                <tbody>
                @if(@isset($blacklists) and @count($blacklists))
                    <?php $i = 0 ?>
                    @foreach($blacklists as $blacklist)
                        <tr data-row-id="<?php echo $i; ?>">
                            <td class="text-left">{{ $blacklist->id }}</td>
                            <td class="text-left">{{ $blacklist->msisdn }}</td>
                            <td class="text-left">{{ $blacklist->channel_type }}</td>
                            <td class="text-left">{{ $blacklist->created_at }}</td>


                        </tr>
                        <?php $i++ ?>
                    @endforeach
                @else
                    <tr>
                        <td class="text-left" colspan="4">No Data Found.</td>

                    </tr>

                @endif

                </tbody>
            </table>
            <div class="bootgrid-footer container-fluid" id="data-table-command-footer">
                <div class="row">
                    <div class="col-sm-6">
                        <?php echo $blacklists->links(); ?>
                    </div>
                </div>
            </div>
        </div>

    </div>
@stop