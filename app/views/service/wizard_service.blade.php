@extends('layouts.master')

@section('content')
    @if(@isset($service))
        @include('common._service_info')
    @endif

<div class="card">
    <div class="card-header">
        <h2>SERVİS TANIMLAMA SİHİRBAZI</h2>
    </div>

    <div class="card-body card-padding">
        <div class="form-wizard-basic fw-container">
            <ul class="tab-nav text-center fw-nav" style="overflow: hidden;" tabindex="12">
                @if(@isset($service))
                    <li class="active"><a aria-expanded="true">SERVIS OLUŞTUR</a></li>
                    <li><a href="/service/ip-list/{{ $service->id }}">IZINLI IP LISTESI</a></li>
                    <li><a href="/service/channel-types/{{ $service->id }}">BLACKLIST TIPI</a></li>
                @else
                    <li class="active"><a aria-expanded="true">SERVIS OLUŞTUR</a></li>
                    <li><a href="#">IZINLI IP LISTESI</a></li>
                    <li><a href="#">BLACKLIST TIPI</a></li>
                @endif

            </ul>

            <div class="tab-content">
                <div id="tab1" class="tab-pane fade active in">
                    @if(@isset($service))
                        @include('service.update')
                    @else
                        @include('service.create')
                    @endif

                </div>
                <div id="tab2" class="tab-pane fade">
                </div>
                <div id="tab3" class="tab-pane fade">
                </div>

            </div>
        </div>
    </div>
</div>

@stop