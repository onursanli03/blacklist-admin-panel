@extends('layouts.master')

@section('content')

    @include('common._service_info')
<div class="card">
    <div class="card-header">
        <h2>SERVİS TANIMLAMA SİHİRBAZI</h2>
    </div>

    <div class="card-body card-padding">
        <div class="form-wizard-basic fw-container">
            <ul class="tab-nav text-center fw-nav" style="overflow: hidden;" tabindex="12">
                @if(@isset($service))
                    <li href="/service/update/{{ $service->id }}"><a >SERVIS OLUŞTUR</a></li>
                    <li><a href="/service/ip-list/{{ $service->id }}">IZINLI IP LISTESI</a></li>
                    <li class="active"><a href="/service/channel-types/{{ $service->id }}" aria-expanded="true">BLACKLIST TIPI</a></li>
                @else
                    <li ><a href="#">SERVIS OLUŞTUR</a></li>
                    <li><a href="#">IZINLI IP LISTESI</a></li>
                    <li class="active"><a href="#"  aria-expanded="true">BLACKLIST TIPI</a></li>
                @endif

            </ul>

            <div class="tab-content">
                <div id="tab1" class="tab-pane fade">
                </div>
                <div id="tab2" class="tab-pane fade">
                </div>
                <div id="tab3" class="tab-pane fade active in">
                    {{ Form::model($service, array('url' => array('service/channel-types', $service->id))) }}
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <div class="fg-line">
                                    <div class="select">
                                        <select class="form-control" name="channel_type_id">
                                            <option>Seçiniz</option>
                                            @if(@isset($allChannelTypes))
                                                @foreach($allChannelTypes as $channelType)
                                                    <option value="{{$channelType->id}}">{{$channelType->channel_type}}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-3">
                            <p>{{ Form::submit('Ekle', array('class' => 'btn btn-primary')) }}</p>
                        </div>
                    </div>

                    {{ Form::close() }}
                </div>

            </div>
        </div>
        <div class="row">
            <div class="col-sm-1 pull-right">
                <a href="/dashboard/index" class="btn bgm-teal waves-effect">TAMAMLA</a>
            </div>
        </div>
    </div>
</div>


<div class="ip-list">
    <div class="card">
        <div class="card-header">
            <div class="col-sm-6">
                <h2>Blacklist Tip Listesi</h2>
            </div>
        </div>

        <table class="table table-striped table-vmiddle bootgrid-table" id="data-table-command" aria-busy="false">
            <thead>
            <tr>
                <th class="text-left" data-column-id="id"><a class="column-header-anchor"
                                                             href="javascript:void(0);"><span class="text">ID</span><span
                                class="md icon "></span></a></th>
                <th class="text-left" data-column-id="sender"><a class="column-header-anchor"
                                                                 href="javascript:void(0);"><span class="text">Tip</span><span
                                class="md icon "></span></a></th>
                <th class="text-left" data-column-id="commands"><a class="column-header-anchor "
                                                                   href="javascript:void(0);"><span class="text">Oluşturma Tarihi</span><span
                                class="md icon "></span></a></th>
            </tr>
            </thead>
            <tbody>
            @if(@isset($channelTypes) and @count($channelTypes))
                <?php $i = 0 ?>
                @foreach($channelTypes as $channelType)
                    <tr data-row-id="<?php echo $i; ?>">
                        <td class="text-left">{{ $channelType->id }}</td>
                        <td class="text-left">{{ $channelType->channel_type}}</td>
                        <td class="text-left">{{ $channelType->created_at}}</td>
                        <td class="text-left">
                            <a href="/service/delete-channel-type/{{ $channelType->id }}" data-row-id="{{$channelType->id}}" class="btn btn-icon btn-danger">
                                <span class="md md-delete m-t-5"></span>
                            </a>
                        </td>
                    </tr>
                    <?php $i++ ?>
                @endforeach
            @else
                <tr>
                    <td class="text-left" colspan="4">No Data Found.</td>

                </tr>

            @endif

            </tbody>
        </table>
        <div class="bootgrid-footer container-fluid" id="data-table-command-footer">
            <div class="row">
                <div class="col-sm-6">
                    <?php echo $channelTypes->links(); ?>
                </div>
            </div>
        </div>
    </div>

</div>
@stop