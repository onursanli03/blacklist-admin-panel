@extends('layouts.master')

@section('content')
    @include('common._service_info')
<div class="card">
    <div class="card-header">
        <h2>SERVİS TANIMLAMA SİHİRBAZI</h2>
    </div>

    <div class="card-body card-padding">
        <div class="form-wizard-basic fw-container">
            <ul class="tab-nav text-center fw-nav" style="overflow: hidden;" tabindex="12">
                @if(@isset($service))
                    <li><a href="/service/update/{{ $service->id }}" >SERVIS OLUŞTUR</a></li>
                    <li class="active"><a aria-expanded="true" href="/service/ip-list/{{ $service->id }}">IZINLI IP LISTESI</a></li>
                    <li><a href="/service/channel-types/{{ $service->id }}">BLACKLIST TIPI</a></li>
                @else
                    <li><a href="#">SERVIS OLUŞTUR</a></li>
                    <li class="active"><a href="#"  aria-expanded="true">IZINLI IP LISTESI</a></li>
                    <li><a href="#">BLACKLIST TIPI</a></li>
                @endif
            </ul>

            <div class="tab-content">
                <div id="tab1" class="tab-pane fade">

                </div>
                <div id="tab2" class="tab-pane fade  active in">
                    {{ Form::model($service, array('url' => array('service/ip-list', $service->id))) }}
                   <div class="row">
                       <div class="col-sm-3">
                           <div class="form-group">
                               {{ Form::label('ip', 'IP ADRESI') }}
                               {{ Form::text('ip', Input::old('ip'), array('autofocus' => true, 'class' => 'form-control', 'placeholder' => '   192.168.1.1')) }}
                               <div class="form-error">{{ $errors->first('ip') }}</div>
                           </div>
                       </div>
                       <div class="col-sm-3 m-t-20">
                           <p>{{ Form::submit('Ekle', array('class' => 'btn btn-primary')) }}</p>
                       </div>
                   </div>

                    {{ Form::close() }}
                </div>
                <div id="tab3" class="tab-pane fade">
                </div>

            </div>
        </div>
        <div class="row">
            <div class="col-sm-1 pull-right">
                <a href="/service/channel-types/{{ $service->id }}" class="btn bgm-cyan waves-effect">SONRAKI</a>
            </div>
        </div>
    </div>
</div>

<div class="ip-list">
    <div class="card">
        <div class="card-header">
            <div class="col-sm-6">
                <h2>Servis IP Listesi</h2>
            </div>
        </div>

        <table class="table table-striped table-vmiddle bootgrid-table" id="data-table-command" aria-busy="false">
            <thead>
            <tr>
                <th class="text-left" data-column-id="id"><a class="column-header-anchor"
                                                             href="javascript:void(0);"><span class="text">ID</span><span
                                class="md icon "></span></a></th>
                <th class="text-left" data-column-id="sender"><a class="column-header-anchor"
                                                                 href="javascript:void(0);"><span class="text">IP</span><span
                                class="md icon "></span></a></th>
                <th class="text-left" data-column-id="commands"><a class="column-header-anchor "
                                                                   href="javascript:void(0);"><span class="text">Oluşturma Tarihi</span><span
                                class="md icon "></span></a></th>
            </tr>
            </thead>
            <tbody>
            @if(@isset($ipList) and @count($ipList))
                <?php $i = 0 ?>
                @foreach($ipList as $ip)
                    <tr data-row-id="<?php echo $i; ?>">
                        <td class="text-left">{{ $ip->id }}</td>
                        <td class="text-left">{{ $ip->ip }}</td>
                        <td class="text-left">{{ $ip->created_at }}</td>
                        <td class="text-left">
                        <td class="text-left">
                            <a href="/service/delete-ip/{{ $ip->id }}" data-row-id="{{$ip->id}}" class="btn btn-icon btn-danger">
                                <span class="md md-delete m-t-5"></span>
                            </a>
                        </td>
                    </tr>
                    <?php $i++ ?>
                @endforeach
            @else
                <tr>
                    <td class="text-left" colspan="4">No Data Found.</td>

                </tr>

            @endif

            </tbody>
        </table>
        <div class="bootgrid-footer container-fluid" id="data-table-command-footer">
            <div class="row">
                <div class="col-sm-6">
                    <?php echo $ipList->links(); ?>
                </div>
            </div>
        </div>
    </div>

</div>

@stop