<div class="card">
    <div class="card-header">

    </div>

    <div class="card-body card-padding">
        {{ Form::open(array('url' => 'service/create'))}}
        @include('service._form')
            <div class="row">
                <div class="col-sm-3" style="margin-left: 30px;">
                    <p>{{ Form::submit('Ekle', array('class' => 'btn btn-primary')) }}</p>
                </div>
            </div>
        {{ Form::close() }}
    </div>
</div>

