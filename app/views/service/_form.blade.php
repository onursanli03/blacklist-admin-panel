<div class="col-md-12">

    <div class="form-group">
        {{ Form::label('name', 'SERVİS ADI') }}
        {{ Form::text('name', Input::old('name'), array('autofocus' => true, 'class' => 'form-control')) }}
        <div class="form-error">{{ $errors->first('name') }}</div>
    </div>
    <div class="form-group">
        {{ Form::label('username', 'KULLANICI ADI') }}
        {{ Form::text('username', Input::old('username'), array('autofocus' => true, 'class' => 'form-control')) }}
        <div class="form-error">{{ $errors->first('username') }}</div>
    </div>
    <div class="form-group">
        {{ Form::label('password', 'PAROLA') }}
        {{ Form::password('password', array('class' => 'form-control')) }}
        <div class="form-error">{{ $errors->first('password') }}</div>
    </div>
</div>


