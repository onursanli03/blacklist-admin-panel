<?php


/**
 * Class ChannelTypes
 */
class ChannelTypes extends Eloquent {


    public $guarded = array();
    
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'CHANNEL_TYPES';


}
