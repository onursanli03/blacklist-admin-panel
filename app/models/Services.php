<?php


/**
 * Class Blacklist
 */
class Services extends Eloquent {


    public $guarded = array('updated_at', 'created_at');

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'SERVICES';


}
