<?php

/**
 * Class ExcelImportLogs
 */
class ExcelImportLogs extends CreatableBaseItem {

    const UPLOADED = 'UPLOADED';

    const IMPORTING = 'IMPORTING';

    const IMPORTED = 'IMPORTED';

    const FAILED = 'FAILED';

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'EXCEL_IMPORT_LOG';


    public static function getInstance($filepath){
        $instance = new ExcelImportLogs();
        $instance->file_path = $filepath;
        $instance->row_count = 0;
        return $instance;
    }

}
