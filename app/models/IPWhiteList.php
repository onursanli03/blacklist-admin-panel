<?php


/**
 * Class IPWhiteList
 */
class IPWhiteList extends Eloquent {


    public $guarded = array();

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'IP_WHITE_LIST';


}
