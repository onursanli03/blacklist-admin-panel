<?php
/**
 * Created by IntelliJ IDEA.
 * User: gdsl
 * Date: 2/27/15
 * Time: 4:57 PM
 */

class CreatableBaseItem extends BaseItem {

    function __construct(){
        parent::__construct();
        $this->guarded = array_merge($this->guarded, array('created_at', 'created_by'));
    }

    public static function boot()
    {
        parent::boot();

        static::creating(function($post)
        {
            $post->created_by = Sentry::getUser()->id;
            $post->created_at = time();
        });
    }
}