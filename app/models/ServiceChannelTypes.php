<?php


/**
 * Class ServiceChannelTypes
 */
class ServiceChannelTypes extends Eloquent {


    public $guarded = array();
    
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'SERVICE_CHANNEL_LIST';


}
