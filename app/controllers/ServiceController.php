<?php

/**
 * Class ServiceController
 */
class ServiceController extends BaseController {




	/*|--------------------------------------------------------------------------|*/
	public function getCreate(){
		return View::make('service.wizard_service');
	}


	/*|--------------------------------------------------------------------------|*/
	public function postCreate(){
		return $this->save(new Services());
	}


	public function getUpdate($id)
	{
		$service = Services::findOrFail($id);
		$service->password = '';
		return View::make('service.wizard_service')->with(array(
			'service' => $service,
		));
	}

	public function putUpdate($id)
	{
		$service = Services::findOrFail($id);
		return $this->save($service);
	}


	private function save(Services $service){
		// validate the info, create rules for the inputs
		$rules = array(
			'name'  => 'required',
			'username' => 'required',
			'password' => 'required',

		);
		// run the validation rules on the inputs from the form
		$validator = Validator::make(Input::all(), $rules);

		// if the validator fails, redirect back to the form
		if ($validator->fails()) {
			return Redirect::to('/service/create')
			->withErrors($validator) // send back all errors to the login form
			->withInput(Input::except('password')) // send back the input (not the password) so that we can repopulate the form
			->with('alert', 'Required fields are missing.');
		} else {
			$inputs = Input::except('_token');

			$service->fill($inputs);

			$service->save();

			return Redirect::to('/service/ip-list/'.$service->id)->with('success',  Config::get('messages.tr')['process.success']);
		}
	}

	public function getIpList($serviceId){
		$service = Services::findOrFail($serviceId);
		$ipList = IPWhiteList::where('service_id', '=', $service->id)->paginate(10);
		return View::make('service.wizard_ip')->with(array(
			'service' => $service,
			'ipList' => $ipList,
		));
	}


	public function postIpList($serviceId){

		// validate the info, create rules for the inputs
		$rules = array(
			'ip'  => 'required',
		);
		// run the validation rules on the inputs from the form
		$validator = Validator::make(Input::all(), $rules);

		// if the validator fails, redirect back to the form
		if ($validator->fails()) {
			return Redirect::to('/service/ip-list')
			->withErrors($validator) // send back all errors to the login form
			->with('alert', 'Required fields are missing.');
		} else {

			$service = Services::findOrFail($serviceId);
			$ip = Input::get('ip');

			if(IPWhiteList::where('service_id', '=', $service->id)->where('ip', '=', $ip)->exists()){
				return Redirect::to('/service/ip-list/'.$service->id)->with('alert',  Config::get('messages.tr')['already.exist']);
			}else{
				$ipWhite['service_id'] = $service->id;
				$ipWhite['ip'] = $ip;

				IPWhiteList::insert($ipWhite);

				return Redirect::to('/service/ip-list/'.$service->id)->with('success',  Config::get('messages.tr')['process.success']);
			}
		}
	}

	public function getDeleteIP($ipId){
		$ip = IPWhiteList::findOrFail($ipId);
		$ip->delete();
		return Redirect::to('/service/ip-list/'.$ip->service_id)->with('success',  Config::get('messages.tr')['process.success']);
	}





	/**************************************************************************/

	public function getChannelTypes($serviceId){
		$service = Services::findOrFail($serviceId);

		$channelTypes = DB::table('service_channel_list')
		->join('channel_types', 'service_channel_list.channel_type_id', '=', 'channel_types.id')
		->select('channel_types.channel_type', 'service_channel_list.id', 'service_channel_list.created_at')
			->where('service_channel_list.service_id', '=', $service->id)
		->paginate(10);

		$allChannelTypes = ChannelTypes::all();

		return View::make('service.wizard_channel_types')->with(array(
			'service' => $service,
			'channelTypes' => $channelTypes,
			'allChannelTypes' => $allChannelTypes,
		));
	}


	public function postChannelTypes($serviceId){

		// validate the info, create rules for the inputs
		$rules = array(
			'channel_type_id'  => 'required|integer',
		);
		// run the validation rules on the inputs from the form
		$validator = Validator::make(Input::all(), $rules);

		// if the validator fails, redirect back to the form
		if ($validator->fails()) {
			return Redirect::to('/service/channel-types')
			->withErrors($validator) // send back all errors to the login form
			->with('alert', 'Required fields are missing.');
		} else {

			$service = Services::findOrFail($serviceId);
			$channelType = ChannelTypes::findOrFail(Input::get('channel_type_id'));

			if(ServiceChannelTypes::where('service_id', '=', $service->id)->where('channel_type_id', '=', $channelType->id)->exists()){
				return Redirect::to('/service/channel-types/'.$service->id)->with('alert',  Config::get('messages.tr')['already.exist']);

			}else{
				$serviceChannelType['service_id'] = $service->id;
				$serviceChannelType['channel_type_id'] = $channelType->id;
				$serviceChannelType['status'] = EntityStatus::ACTIVE;

				ServiceChannelTypes::insert($serviceChannelType);

				return Redirect::to('/service/channel-types/'.$service->id)->with('success',  Config::get('messages.tr')['process.success']);
			}
		}
	}

	public function getDeleteChannelType($channelTypeId){
		$serviceChannelType = ServiceChannelTypes::findOrFail($channelTypeId);
		$serviceChannelType->delete();
		return Redirect::to('/service/channel-types/'.$serviceChannelType->service_id)->with('success',  Config::get('messages.tr')['process.success']);
	}



}
