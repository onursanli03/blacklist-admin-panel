<?php

/**
 * Class HomeController
 */
class HomeController extends BaseController {


	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/
    /*|--------------------------------------------------------------------------|*/
	public function showWelcome()
	{
        $this->layout->content = View::make('/site/login');
	}

    /*|--------------------------------------------------------------------------|*/
    public function getLogin()
    {
        // already logged in
        if (Sentry::check()){
            return Redirect::to('/dashboard/index');
        }
        return View::make('/site/login');
    }

    /*|--------------------------------------------------------------------------|*/
    public function postLogin()
    {
        $result = array('status' => 'failed', 'status-message' => 'FAILED');
        try
        {

            // Login credentials
            $credentials = array(
                'email'    => Input::get('email'),
                'password' => Input::get('password'),
            );

            // Authenticate the user
            $user = Sentry::authenticate($credentials, false);
            return Redirect::intended("dashboard/index");
        }
        catch (Cartalyst\Sentry\Users\LoginRequiredException $e)
        {
            $result = array('status' => 'failed', 'status-message' => 'Username and password are required.');
        }
        catch (Cartalyst\Sentry\Users\PasswordRequiredException $e)
        {
            $result = array('status' => 'failed', 'status-message' => 'Password field is required.');
        }
        catch (Cartalyst\Sentry\Users\WrongPasswordException $e)
        {
            $result = array('status' => 'failed', 'status-message' => 'Wrong password, try again.');

        }
        catch (Cartalyst\Sentry\Users\UserNotFoundException $e)
        {
            $result = array('status' => 'failed', 'status-message' => 'User was not found.');
        }
        catch (Cartalyst\Sentry\Users\UserNotActivatedException $e)
        {
            $result = array('status' => 'failed', 'status-message' => 'User is not activated.');
        }

        // The following is only required if the throttling is enabled
        catch (Cartalyst\Sentry\Throttling\UserSuspendedException $e)
        {
            $result = array('status' => 'failed', 'status-message' => 'User is suspended.');
        }
        catch (Cartalyst\Sentry\Throttling\UserBannedException $e)
        {
            $result = array('status' => 'failed', 'status-message' => 'User is banned.');
        }

        return View::make('site/login')->with(array(
            'result' => $result,
        ));

    }


    /*|--------------------------------------------------------------------------|*/
    public function postLogout()
    {
        Sentry::logout();
        return Redirect::to('/site/login'); // redirect the user to the login screen
    }

    /*|--------------------------------------------------------------------------|*/
    public function getLogout()
    {
        Sentry::logout();
        return Redirect::to('/site/login'); // redirect the user to the login screen
    }



}
