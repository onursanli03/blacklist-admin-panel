<?php

/**
 * Class ServiceController
 */
class BlacklistController extends BaseController
{

    private static $EXCEL_IMPORT = "EXCEL_IMPORT";

    private static $MANUEL = "MANUEL";


    /*|--------------------------------------------------------------------------|*/
    public function search($serviceId)
    {

        $service = Services::find($serviceId);

        if (Input::has('msisdn')) {
            $blacklists = BlacklistDetailView::where('service_id', '=', $serviceId)->where('msisdn', 'like', '%' . Input::get('msisdn') . '%')->paginate(10);
        } else {
            $blacklists = BlacklistDetailView::where('service_id', '=', $serviceId)->paginate(10);
        }
        return View::make('blacklist.list')->with(array(
            'blacklists' => $blacklists,
            'service' => $service
        ));
    }


    /*|--------------------------------------------------------------------------|*/
    public function getList($serviceId)
    {
        $service = Services::findOrFail($serviceId);
        $blacklists = BlacklistDetailView::where('service_id', '=', $serviceId)->paginate(10);
        return View::make('blacklist.list')->with(array(
            'blacklists' => $blacklists,
            'service' => $service
        ));
    }


    /*|--------------------------------------------------------------------------|*/
    public function getExcelImport($serviceId)
    {
        $service = Services::findOrFail($serviceId);
        return View::make('blacklist.import')->with(array(
            'excel_import_logs' => ExcelImportLogs::orderBy('created_at', 'desc')->paginate(10),
            'service' => $service
        ));
    }


    /*|--------------------------------------------------------------------------|*/
    public function postExcelImport($serviceId)
    {

        $excelImportLog = null;

        try {
            $service = Services::findOrFail($serviceId);

            $channelType = ChannelTypes::where('channel_type', '=', $this::$EXCEL_IMPORT)->firstOrFail();

            if (!Input::hasFile('excel')) {
                return Redirect::to('blacklist/excel-import')->with('alert', 'Excel ekleyiniz.');
            }


            $unique_folder = md5(uniqid());

            $absolutePath = '/tmp/' . $unique_folder;


            if (!file_exists($absolutePath)) {
                mkdir($absolutePath, 0777, true);
            }

            $file = Input::file('excel');

            $file_path = $absolutePath . '/' . $file->getFilename();
            $file->move($absolutePath);

            $excelImportLog = ExcelImportLogs::getInstance($file_path);
            $excelImportLog->import_status = ExcelImportLogs::IMPORTING;
            $excelImportLog->row_count = 0;
            $excelImportLog->save();

            $objReader = PHPExcel_IOFactory::createReaderForFile($excelImportLog->file_path);
            $objPHPExcel = $objReader->load($excelImportLog->file_path);

            $data = array();

            for ($i = 0; $i < $objPHPExcel->getSheetCount(); $i++) {
                $highestColumn = $objPHPExcel->setActiveSheetIndex($i)->getHighestColumn();
                $highestRow = $objPHPExcel->setActiveSheetIndex($i)->getHighestRow();

                echo 'getHighestColumn() =  [' . $highestColumn . ']<br/>';
                echo 'getHighestRow() =  [' . $highestRow . ']<br/>';

                echo '<table border="1">';
                foreach ($objPHPExcel->setActiveSheetIndex($i)->getRowIterator() as $row) {

                    $cellIterator = $row->getCellIterator();
                    $cellIterator->setIterateOnlyExistingCells(false);
                    foreach ($cellIterator as $cell) {
                        if (!is_null($cell)) {

                            $msisdn = $cell->getCalculatedValue();
                            try {
                                if (Helper::isMsisdnValid($msisdn)) {
                                    $temp['msisdn'] = $msisdn;
                                    $temp['service_id'] = $service->id;
                                    $temp['channel_type_id'] = $channelType->id;
                                    $temp['status'] = EntityStatus::ACTIVE;
                                    $temp['excel_import_id'] = $excelImportLog->id;
                                    $data[] = $temp;
                                    $excelImportLog->row_count++;
                                }

                            } catch (Exception $ex) {
                                $excelImportLog->save();
                                Log::error('Error occurred while inserting blacklist. ' . $ex->getMessage());
                            }
                            if (count($data) > 900) {
                                Blacklist::insert($data);
                                $excelImportLog->save();
                                $data = array();
                            }
                        }
                    }
                }
            }

            if (count($data) > 0) {
                Blacklist::insert($data);
                $excelImportLog->save();
            }

            $excelImportLog->import_status = ExcelImportLogs::IMPORTED;
            $excelImportLog->save();
            return Redirect::to('blacklist/excel-import/' . $service->id)->with('success', Config::get('messages.tr')['process.success']. " " . count($data) ." msisdn eklendi.");
        } catch (Exception $ex) {
            if ($excelImportLog != null) {
                $excelImportLog->import_status = ExcelImportLogs::FAILED;
                $excelImportLog->save();
            }
            return Redirect::to('dashboard/index')->with('alert', Config::get('messages.tr')['process.fail']);
        }
    }

    /***********************************************************************************/

    /*|--------------------------------------------------------------------------|*/
    public function getCreate($serviceId)
    {
        $service = Services::findOrFail($serviceId);
        return View::make('blacklist.create')->with(array(
            'service' => $service,
        ));
    }


    /*|--------------------------------------------------------------------------|*/
    public function postCreate($serviceId)
    {
        $service = Services::findOrFail($serviceId);
        return $this->save($service, new Blacklist());
    }


    private function save(Services $service, Blacklist $blacklist)
    {
        // validate the info, create rules for the inputs
        $rules = array(
            'msisdn' => 'required',
        );
        // run the validation rules on the inputs from the form
        $validator = Validator::make(Input::all(), $rules);

        // if the validator fails, redirect back to the form
        if ($validator->fails()) {
            return Redirect::to('/blacklist/create')
                ->withErrors($validator)// send back all errors to the login form
                ->with('alert', 'Required fields are missing.');
        } else {
            $inputs = Input::except('_token');

            $msisdn = $inputs['msisdn'];
            if (Helper::isMsisdnValid($msisdn)) {
                $channelType = ChannelTypes::where('channel_type', '=', $this::$MANUEL)->firstOrFail();
                $blacklist->msisdn = $msisdn;
                $blacklist->service_id = $service->id;
                $blacklist->channel_type_id = $channelType->id;
                $blacklist->save();
                return Redirect::to('/blacklist/create/' . $service->id)->with('success', Config::get('messages.tr')['process.success']);
            } else {
                return Redirect::to('/blacklist/create/' . $service->id)->with('alert', Config::get('messages.tr')['msisdn.not.valid']);
            }


        }
    }

}
