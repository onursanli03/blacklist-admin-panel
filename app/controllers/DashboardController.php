<?php

/**
 * Class DashboardController
 */
class DashboardController extends BaseController {


	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/
    /*|--------------------------------------------------------------------------|*/
	public function getIndex()
	{
		$services = Services::paginate(10);
		$serviceCount = Services::count();
		$channelTypeCount = ChannelTypes::count();
		$blacklistCount = Blacklist::count();


        return View::make('dashboard.index')->with(array(
			'services' => $services,
			'serviceCount' => $serviceCount,
			'channelTypeCount' => $channelTypeCount,
			'blacklistCount' => $blacklistCount,


		));
	}

	/*
	|--------------------------------------------------------------------------
	| Searches
	|--------------------------------------------------------------------------
	/*|--------------------------------------------------------------------------|*/
	public function search()
	{
		if(Input::has('name')){
			$services = Services::where('name', 'like', '%'.Input::get('name').'%')->paginate(10);
		}else{
			$services = Services::paginate(10);
		}

		$serviceCount = Services::count();
		$channelTypeCount = ChannelTypes::count();
		$blacklistCount = Blacklist::count();


		return View::make('dashboard.index')->with(array(
			'services' => $services,
			'serviceCount' => $serviceCount,
			'channelTypeCount' => $channelTypeCount,
			'blacklistCount' => $blacklistCount,

		));
	}



}
