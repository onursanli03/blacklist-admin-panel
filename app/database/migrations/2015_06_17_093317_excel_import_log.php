<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ExcelImportLog extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
		Schema::create('excel_import_log', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('file_path');
			$table->integer('row_count');
			$table->string('import_status');
			$table->timestamps();
			$table->integer('created_by');
		});
		//
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
